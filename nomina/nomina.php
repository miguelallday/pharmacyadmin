<?php
include("../includes/clase-mostrar-elementosde-db.php");
include("../templates/header.php");
session_start();	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" href="nomina.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="../cssgenerico.css" />
<link type=text/css rel=stylesheet href=../estilos/header.css />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>nomina</title>
</head>

<body>

<?php 
$header = new Header();
$header->set($_SESSION['username']);
$directorio = "../index.php";
$header->setDir($directorio);
echo $header->header();
		 ?>
<div id="contenedor-padre">
<div id="nav2">
<div id="registrar"><a href="registrar-empleado.php">Registrar Usuario</a></div>
<div id="buscar"><b>Buscar Empleado</b><form action="buscar-empleado.php" method="post"><input type="text" name="buscar" placeholder="buscar empleado" /><input type="submit" value="buscar" /></form></div>
</div>
<br />
<br />
<p id="itemborrado"><?php if(isset($_SESSION['itemborrado'])&&!empty($_SESSION['itemborrado'])){
	echo "empleado borrado con exito:"." ".$_SESSION['itemborrado'];
	unset($_SESSION['itemborrado']);
	}?></p>
<ul id="encabezado">
  <li id="tags">Nombre</li>
  <li id="tags">Direccion</li>
  <li id="tags">Cedula</li>
</ul>
<br />
<div id="content">
  <?php 
  $num = 3;
  $action = "modificar-empleado.php";
  $action2 = "eliminar-empleado.php";
  //aqui instanceo la clase que me hace la consulta y le paso el script sql para que sepa de donde seleccionar
  $consulta = "SELECT * FROM empleados";
if(isset($_SESSION['username'])){ //aqui compruebo que existe la variable sesion, es decir, que estas logeado
	$ver = new Datagridview();
	$ver->mostrar($consulta, $action, $num, $action2);
	$cerrarsesion = "cerrar sesion";
	$link = "../includes/cerrarsesion.php";
}else{
	echo "Inicia sesion para ver este contenido";
	$cerrarsesion = "iniciar sesion";
	$link = "../index.php";
	
	}

?>
</div>
<!-- esto es para que cuando no estes logeado aparezca un boton con iniciar sesion pero si lo estas que diga cerrar sesion-->
<div id="exit"><a href="<?php echo $link;?>"><?php echo $cerrarsesion;?></a></div>
<div id="volver"><a href="../logeado.php">Volver</a></div>
</div>
</body>
</html>